package com.example.ms25task1.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class HelloController {

    @GetMapping(path = "/hello")
    public ResponseEntity<String> hello() {
        return ResponseEntity
                .ok(
                        "Hello MS-25!"
                );
    }
}
