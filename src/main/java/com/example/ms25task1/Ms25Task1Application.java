package com.example.ms25task1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms25Task1Application {

    public static void main(String[] args) {
        SpringApplication.run(Ms25Task1Application.class, args);
    }

}
